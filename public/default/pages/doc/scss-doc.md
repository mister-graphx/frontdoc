---
title: SASS Doc
---

# SCSS Doc


```
/* # Doc bloc title

Description in markdown extra formating
<http://www.url.me>
	
~~~
exemple_code_usage(){
	//content
}
~~~	

:Hover			-	Description 
.modifier		-	Description
.anotherClass @extend .firstClass - extender description


Markup:
<header>
	<h1 class="$modifierClass"></h1>
</header>

$parameters - 



Styleguide category.sub-categorie.item
*/
```

## Parameter section :

Complete list of tags supported by SassDoc, for reference: <http://sassdoc.com/annotations/>


Supported parameters : [ % | @ | $ ]

The parameter section is rendered as a table with 

| Parameter Name | Parameter description |
| --- | --- |
| $variable | Description of the variable |

In KSS spec, we can add a {type} most of others specs use a (! default) directive wich can be helpful in a description. 

Variables {types} : [ number, string, bool, color, list, map or null ].

### @import

	@import  "compass/css3/" -  Use transition, image, 
	
### $params $variables
	
	$vars	-	{type:boolean|string|int} (! Default) Description for the variable or param

### %placeholder

	%placeholder -

### @mixin 

	@mixin - example_mixin($param, $param)

### @function

	@function - exemple_funtion()

### @extend

	@extend 

### @usage

	@usage - @include example_mixin() or @extend %placeholder or example_function()

### @media

Describe a @media directive

	@media - {$large} describe what happen in $large case

	@media - {min-width:320px max-width:700px } describe what happen
	
	// Breakpoint syntax {min-width max-width}
	@media - {320px 700px } describe what happen
	
	// Default in mobile first 
	@media - {default} html.font-size:90%
	

### @return	
	@return - {type} (! default) description of the fonction return	
	

## Sources & inspirations

[SassDoc](http://www.sitepoint.com/sassdoc-documentation-tool-sass/)
ou 
<https://github.com/eoneill/sassdoc>
    
Spec exemples :
[Java Doc Style dictionary](http://usejsdoc.org/)

Keywords

| key  | meaning   |
|---- |  --- |
|  @author  | author |
| @mixin  		| define a mixin |
| @function	 	| define a function |
|@param		|  a parameter for a method|
|@return		|  what is returned in the method|
|@throw		| exception |
|@private		|  flag a method as private|
|@usage		|  provide a block of example usage|
| *@category*	|  *define a category for the method*  [^?] |
|@link			|  link off to a URL (limited support)|
|@source 		|  Source url |
|@see			|  reference another method (limited support)|


[^?]: KSS Doc



	


	 