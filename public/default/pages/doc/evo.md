---
title: Evolutions
---

## Styleguide


Color system, gestion des palettes de couleur :

Import / Export de palettes au format Scss, Aco,Ase
Délocaliser les variables conçernant la couleur dans un fichiers synchronisable avec les differents éditeurs (Adobe ...)
Pouvoir importer les palettes de colorLovers directement.

Markdown Tools :

<https://packagist.org/packages/pixel418/markdownify>

## Projets:

Pouvoir déployer pour un unique projet

Pouvoir éditer les projets, en ajouter

Déplacer le dossier des configs de projet

Avoir des articles spécifiques aux projet,
déplacer le dossier article de www ? les articles ne doive pas êtres écrasés par une maj avec composer



##  Pages

Project hub:
<https://github.com/tzi/ProjectHub>


MarkupDoc:

A parser and documentation tools for the templates files / markup


## Debug

<http://www.richardcarlier.com/article-146-ajax-et-les-erreurs-invisibles.html>

Comment ça marche : var_export, var_dump :
consigner les erreurs dans des fichiers, rediriger vers la console ...
<http://www.commentcamarche.net/faq/3158-php-methodes-de-debogage>

x-Debug :
<http://fr.openclassrooms.com/informatique/cours/php-utiliser-un-debogueur-pour-php-xdebug>

<http://allevents.avonture.be/fr/item/409-xdebug.html>

[Php profiler mac](http://blog.jcmultimedia.com.au/2014/03/profiling-php-on-osx-with-xdebug-kcachegrind/)

Ajout d'une classe de documentation du code php
<http://cnedelcu.net/phpdocfill/>
