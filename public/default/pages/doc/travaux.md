---
title: Travaux
---


Navigation ne pas afficher le markdown 404

Articles les entetes de fichier markdown sont accessible via {{ page.paramxxx }} dans les templates


## 1.0.1 :

### Ajouts :

fonction Styleguide\markupReplacements - Ajout de chaines remplaçables dans le markup.

Kss-php propose le remplacement de `$modifierClass` dans le markup. On ajoute la possibilitée de décrire des remplacements supplémentaires depuis le fichier de config du projet, avec une entrée `replacements` et des couples chaine:valeur.


Preview Ajout de la prise en charge de webfont google définies dans le projet.

### Correctifs

Javascript taille des iframes de preview, mieux prendre en compte la barre de scroll du navigateur, pour être un peut plus fiable.

## 1.0.0pre

Sun Jul 27 14:22:48 2014
:   *   Correction sur l'affichage des blocs de documentation, quand c'est une sous rubrique

Wed Jul 23 12:07:23 2014
:   *   Doc et corrections sur les docs
    *   Ajout d'un doc pour les TODO


Tue Jul 22 19:17:59 2014
:   *   Ajout du moteur de recherche
    *   Travail sur le js des outils responsive


Wed Jul 16 17:34:55 2014
:   *   Ajout de la gestion des informations du niveau de compatibilité navigateur dans scan\section
    *   Ajout des blocs de rendupour les infos Compatibilité, Expérimental, Déprécié
    *   Ajout d'une iFrame pour la preview pour ne pas casser les style du site de documentation
    *   La prévisualisation du code est faite via Highlight.js et plus prettiffy

Wed Mar 05 10:34:51 2014
:   *   On passe de la nouvelle version de less.php
    *   Ajout de html2pdf pour générer des pdf :
        relativement inutile car les styles ne seront pas intérprétés, s'orienter plutot vers du html ou epub
