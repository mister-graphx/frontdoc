---
title: Documenter les Styles
---

# Documentation des feuilles de style



## Documentation des styles :

[KSS - Syntax 4 documenting CSS](http://warpspire.com/kss/syntax/)

[KSS php - GitHub](https://github.com/scaninc/kss-php)


Exemple de documentation

```css
/** # A button suitable for giving stars to someone.

:hover             - Subtle hover highlight.
.stars-given       - A highlight indicating you've already given a star.
.stars-given:hover - Subtle hover highlight on top of stars-given styling.
.disabled    @extend .btn      - Dims the button to indicate it cannot be used.

Markup:
<a class="button">Click Me</a>

Styleguide 2.1.3.
*/
a.button.star{
  ...
}
a.button.star.stars-given{
  ...
}
a.button.star.disabled{
  ...
}
```


*   La syntaxe de MarkDown Extra est utilisable dans les descriptions de sections
*   On peut utiliser fixie.js pour les images dans le Markup, et les génération de faux textes
    `<img width="300" height="200">`


## Instalation

[Documentation Composer](http://getcomposer.org/doc/)

[Dépost : Packagist](https://packagist.org/)

Commandes
:   Installation d'un projet et de ses dépendance  
    `php composer.phar install`

    Mise a jour d'un projet  
    `php composer.phar update`

    Mettre à jour composer  
    `php composer.phar self-update `

**Librairies utilisées :**

* KSSphp
* Markdown

## Autres sources :

[Prettify - Pour le formatage et la présentation des codes](http://code.google.com/p/google-code-prettify/wiki/GettingStarted)

[Holder.js - Image de remplacements ](https://github.com/imsky/holder)
