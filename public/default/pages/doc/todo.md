---
title: ToDo
---

# Todos !!

- [ ]		Intégrer un filtre de traitement typo, en plugin
	 	<http://kingdesk.com/projects/php-typography/>,
	 	<https://michelf.ca/projects/php-smartypants/>,
	 	<http://contrib.spip.net/Ortho-typographie>

- [ ]		des styles basiques utilisables dans la doc
 		(grille de couleurs, icones)...

- [!]		Intégrer un système de plugins et hooks cf : Pico

- [ ]		Ajouter une gestion multiprojet  

	**Projet**

    Config.json | project.json

		    ```
		    'projet': {
		        'kss_style_paths':{
		            '':''
		        }
		    }
		    ```



- [ ]		Styleguide spécifier le dossier d'analyse ou le fichier via un formulaire plutôt que dans un fichier de config

- [ ]		Intégrer un routing sur app.class, faire un controller front correct :
    		<http://www.sitepoint.com/front-controller-pattern-2/>,
    		<http://fabien.potencier.org/article/50/create-your-own-framework-on-top-of-the-symfony2-components-part-1>

	Si on utilise des composants symphony : httpKernel ?

- [x]		css : Style guide : parameters tables la dernière ligne n'est pas visible cf border-collapse


## Nav

*  [!] Menu : [ENCOURS] récursif sans limite de profondeur
*  [!] La référence demandé , on positionne la navigation.styleguide sur l'item et on la style active

## Code blocks

* [?] Ajouter copyto clipboard sur les blocks de code, markup ?

* [?] export komodo snippet



*  [] Pre-pross : Améliorer la gestion de la documentation pour les mixins
    cf : <doc/scss-doc.md>
    [Scss Doc](index.php?page=article&file=scss-doc.md)



*   TODO : ? Si un  fichier `*.md` est présent a la racine  du sous dossier : **proposer de le visualiser**
    Via un link et popup ... view readme.md            

*   TODO : ? Proposer par defaut une iframe de preview


*   TODO : ? preview/edition du code avec codeMirror

*   TODO : chargement async des iframes
