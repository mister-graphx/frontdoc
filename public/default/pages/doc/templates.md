---
title: Templates
---

## Sources & références

Mustache tags : https://github.com/bobthecow/mustache.php/wiki/Mustache-Tags

## Datas

| Template variable | Description |
|--|--|
| `{{ config.xxx }}` | la config du projet en cours |
| `{{projects}}` | liste des projets |
|`{{ page.xxx }} ` | frontmatter du fichier markdown |


## Filters

our pouvoir utiliser les filtres dans un template, il faut charger les filtres `{{%FILTERS}}`.

https://github.com/bobthecow/mustache.php/wiki/FILTERS-pragma

| Filtre | Description |
|--|--|
| `|markdown` | rendu markdown |
| `{{ greeting | case.lower }}` |  passe en minuscule la chaine |
| `{{ greeting | case.upper }}` |  passe en majuscule la chaine |
