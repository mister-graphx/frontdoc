---
title: Frontdoc
description: "Front doc project Starter"
excerpt: "A  sample project"
---



## Sources & références :

[KSS - Syntax 4 documenting CSS](http://warpspire.com/kss/syntax/)

[KSS php - GitHub](https://github.com/scaninc/kss-php)

### Exemple de styleguides

Réalisé avec kss-node :
https://karlkaufmann.com/themes/custom/kk_theme/styleguide/section-base.html

## Blocs de documentation

Pour que la section soit prise en compte elle doit comporter au minimum

*   Un titre `# Titre de la section`
*   Un index de table des matière : `Styleguide xx.yy.zz`

Exemple de documentation

~~~
/** # A button suitable for giving stars to someone.

:hover             - Subtle hover highlight.
.stars-given       - A highlight indicating you've already given a star.
.stars-given:hover - Subtle hover highlight on top of stars-given styling.
.disabled          - Dims the button to indicate it cannot be used.

Markup:
<a class="button">Click Me</a>

Styleguide 2.1.3.
*/
a.button.star{
  ...
}
a.button.star.stars-given{
  ...
}
a.button.star.disabled{
  ...
}

Styleguide 2.1.1
*/
~~~

## Fonctionalitées

**Images et textes de démonstration**

Frontdoc utilise fixie.js pour les images et les faux textes, ce qui permet
de ne pas remplir le markup avec des textes qui pourraient êtres à supprimer par la suite lors szq phases suivantes de l'intégration.

Dans la prévisualisation du html, les balises h*,p,a,ul,ol,section,article ... sont automatiquements remplies. Les balise `<i>` ayant une classe css icn ou icon sont echapées.

Pour les images on peut spécifier la taille voulue en remplissant les propriété width et height une image de remplacement indiquant les dimensions de l'image sera généré avec le service : http://placehold.it/.
Si on remplit le title le texte sera affiché a la place des dimentions.

On peut spécifier d'autres options comme la couleur de fond et la couleur du texte générique
en surchargeant lors de l'initialisation de fixie avec la méthode `setImagePlaceHolder()` dans le fichier `www/preview.html`

```
// Changer la couleur (toujours après la taille et sans # )
fixie.setImagePlaceholder('http://placehold.it/${w}x${h}/000/fff/&text=${text}').init();
```

On peut aussi spécifié une image de démo par défaut aléatoire :
`fixie.setImagePlaceholder('http://flickholdr.com/${w}/${h}/tag').init();`
ira piocher une image de la taille demandé dans thème /tag sur le site Flickholdr.

Autres exemples d'utilisation : (http://flickholdr.com)

w/h : `http://flickholdr.com/200/300`  
or with tags: `http://flickholdr.com/200/300/sea,sun`  
B&W: `http://flickholdr.com/200/300/sea,sun/bw`  
offsets: `http://flickholdr.com/200/300/sea,sun/1`  

ou sur : http://lorempixel.com/
`fixie.setImagePlaceholder('http://lorempixel.com/${w}/${h}/${text}').init();`


**Mise en forme des descriptions**

La syntaxe de MarkDown Extra est utilisable dans les descriptions de sections.

Liens
:   `<http://www.monsite.tld>` ou `[Texte du lien](http://url_du_site.tld)`

Listes et définitions
:   Le parser, de KSS, empèche l'utilisation des listes non numérotées classiques :
    on utilise donc les listes numérotées ou les définitions

    ```
    1. Item
    2. Item
    3. Item

    ```
    ```
    Titre de la définition
    :   Explication de la définition

    Titre de la deuxième définition
    :   Explication de la deuxième définition

        Sur plusieures lignes

    ```

Inssérer un exemple de code:

Pour insérer un bloc de code on utilise la syntaxe. éviter le . sur une class

    ~~~
        example-selector{
            color: lighten($color,10%);
        }
    ~~~

Placez de préférence le bloc d'usage en fin de Styleguide.


**Modifiers**

*.class ou :etat qui modifie l'objet décris*  
A chaque "modifier" ajouté, un bloc de prévisualisation est ajouté, pour montrer la modification qu'il apporte.

La syntaxe est sans indentation  `<modifier|class|pseudo-element> - Description pour le modifier`

    :hover    - Highlights when hovering.
    :disabled - Dims the button when disabled.
    .primary  - Indicates button is the primary action.
    .smaller  - A smaller button
    .classExtender @extend .btn - Extender definition

`$modifierClass` permet de faire appel a la .class du "modifier".

Exemple : `Markup: <button class="$modifierClass">Example Button</button>`

**Pré-processeurs**

Quand on utilise un pré-processeur css, comme scss ou less , on peut documenter les mixins :

    // Creates a linear gradient background, from top to bottom.
    //
    // $start - The color hex at the top.
    // $end   - The color hex at the bottom.
    //
    // Compatible in IE6+, Firefox 2+, Safari 4+.
    @mixin gradient($start, $end){
      ...
    }

on documentera alors les paramètres et le type de valeur attendue (hex, rvb, nombre, ...).

**Compatibilité navigateur**

    // Compatibility: IE6+, Firefox 2+, Safari 4+.

Si la compatibilité n'as pas été testé :

    // Compatibility: untested.



## Instalation

[Documentation Composer](http://getcomposer.org/doc/)

[Dépot : Packagist](https://packagist.org/)

Commandes :

*   Installation d'un projet et de ses dépendance  
    `php composer.phar install`

*   Mise a jour d'un projet  
    `php composer.phar update`

*   Mettre à jour composer  
    `php composer.phar self-update `

**Librairies utilisées :**

* KSSphp
* ParseDownExtra
* Symphony/Finder
* Mustache

## Présentation des extraits de code

La présentation du code est faite par highlight.js

Si on préfère utiliser google.prettify :
Modifier le markup des blocs et inclure les js et css

~~~

<div class="styleguide__html">
        <pre class="prettyprint linenums:1">
            <code class="lang-html"><?php echo htmlentities($section->getMarkupNormal('{class}')); ?></code>
        </pre>
</div>

~~~


## Autres sources :

[Prettify - Pour le formatage et la présentation des codes](http://code.google.com/p/google-code-prettify/wiki/GettingStarted)

[Highlight](http://highlightjs.org/)
Pakagé sur mesure, avec les langages nécessaires.

[Fixie.js - GitHub](https://github.com/ryhan/fixie)


## A Intégrer

Trouver une présentation visuelle des palette du projet, afin de normaliser l'utilisation des couleurs a travers le site.

<http://flatuicolors.com/>
